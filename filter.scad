/* Copyright 2023 Michal Ulianko */
/* This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA. */

$fa = 0.5;
$fs = 0.5;

wall_thickness = 0.6;
tube_height = 0;
cone_height = 20;
bottom_diameter = 30;
angle = 60;
filter_height = 1;
draw_handle = true;

/* [Hidden] */
total_height = tube_height + cone_height;

function h2dia(h) = bottom_diameter + 2 * tan(angle / 2) * h;
module up(z) translate([0, 0, z]) children();

module base(h1 = 0, h2 = total_height, xoff = 0) {
    d1 = bottom_diameter + 2 * xoff;
    d2 = h2dia(cone_height) + 2 * xoff;

    intersection() {
        union() {
            if (tube_height > 0) {
                cylinder(h = tube_height, d = d1);
            }

            if (cone_height > 0) {
                up(tube_height) cylinder(h = cone_height, d1 = d1, d2 = d2);
            }
        }

        up(h1) linear_extrude(h2 - h1) square([100, 100], center = true);
    }
}

module mirror_copy(v) {
    children();
    mirror(v) children();
}

module handle(r = 5) {
    rotate([90, 0, 0])
    linear_extrude(6, center = true)
    mirror_copy([1, 0])
    difference() {
        square([r + wall_thickness, total_height]);
        translate([r + wall_thickness, r]) circle(r);
        translate([wall_thickness, r]) square(100);
    }
}

module filter() {
    difference() {
        base();
        base(h1 = filter_height, xoff = -wall_thickness);
    }

    if (draw_handle) {
        handle();
    }
}

filter();
